# Czech translation of apt-listbugs.
# Copyright (C) 2002-2020 apt-listbugs authors
# This file is distributed under the same license as the apt-listbugs package.
# Miroslav Kure <kurem@debian.cz>, 2005, 2009, 2012, 2014, 2016, 2019, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: apt-listbugs 0.1.34\n"
"Report-Msgid-Bugs-To: invernomuto@paranoici.org\n"
"POT-Creation-Date: 2022-10-22 17:16+0200\n"
"PO-Revision-Date: 2020-11-09 21:04+0100\n"
"Last-Translator: Miroslav Kure <kurem@debian.cz>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. TRANSLATORS: "E: " is a label for error messages; you may translate it with a suitable abbreviation of the word "error"
#: ../bin/apt-listbugs:429 ../bin/apt-listbugs:462 ../bin/apt-listbugs:467
#: ../bin/apt-listbugs:473 ../bin/apt-listbugs:487 ../bin/apt-listbugs:517
#: ../bin/apt-listbugs:548 ../bin/apt-listbugs:597 ../bin/apt-listbugs:610
#: ../lib/aptlistbugs/logic.rb:310 ../lib/aptlistbugs/logic.rb:320
#: ../lib/aptlistbugs/logic.rb:1072 ../lib/aptlistbugs/logic.rb:1084
#: ../lib/aptlistbugs/logic.rb:1097 ../libexec/aptcleanup:52
#: ../libexec/aptcleanup:55
msgid "E: "
msgstr "E: "

#: ../bin/apt-listbugs:430
msgid ""
"This may be caused by a package lacking support for the ruby interpreter in "
"use. Try to fix the situation with the following commands:"
msgstr ""
"To může být způsobeno balíkem bez podpory použitého interpretru ruby. Zkuste "
"vzniklou situaci vyřešit těmito příkazy:"

#: ../bin/apt-listbugs:462
msgid ""
"APT_HOOK_INFO_FD is undefined.\n"
msgstr ""
"APT_HOOK_INFO_FD není definovaná.\n"

#: ../bin/apt-listbugs:467
msgid ""
"APT_HOOK_INFO_FD is not correctly defined.\n"
msgstr ""
"APT_HOOK_INFO_FD není definovaná správně.\n"

#: ../bin/apt-listbugs:473
msgid "Cannot read from file descriptor %d"
msgstr "Nelze číst z deskriptoru souboru %d"

#: ../bin/apt-listbugs:487
msgid ""
"APT Pre-Install-Pkgs failed to provide the expected 'VERSION 3' string.\n"
msgstr ""
"APT Pre-Install-Pkgs nevrátil očekávaný řetězec „VERSION 3“.\n"

#: ../bin/apt-listbugs:517
msgid ""
"APT Pre-Install-Pkgs provided fewer fields than expected.\n"
msgstr ""
"APT Pre-Install-Pkgs vrátil méně polí, než je očekáváno.\n"

#: ../bin/apt-listbugs:548
msgid ""
"APT Pre-Install-Pkgs provided an invalid direction of version change.\n"
msgstr ""
"APT Pre-Install-Pkgs vrátil neplatný směr změny verze.\n"

#: ../bin/apt-listbugs:627
msgid "****** Exiting with an error in order to stop the installation. ******"
msgstr "********** Ukončeno s chybou s cílem ukončit instalaci. **********"

#: ../lib/aptlistbugs/logic.rb:49
msgid "Usage: "
msgstr "Použití: "

#: ../lib/aptlistbugs/logic.rb:50
msgid " [options] <command> [arguments]"
msgstr " [volby] <příkaz> [argumenty]"

#: ../lib/aptlistbugs/logic.rb:52
msgid ""
"Options:\n"
msgstr ""
"Volby:\n"

#. TRANSLATORS: the colons (:) in the following strings are vertically aligned, please keep their alignment consistent
#. TRANSLATORS: the \"all\" between quotes should not be translated
#: ../lib/aptlistbugs/logic.rb:55
msgid ""
" -s <severities>  : Filter bugs by severities you want to see\n"
"                    (or \"all\" for all)\n"
"                    [%s].\n"
msgstr ""
" -s <závažnosti>  : Zobrazí jen chyby daných závažností\n"
"                    (nebo „all“ pro všechny)\n"
"                    [%s].\n"

#: ../lib/aptlistbugs/logic.rb:56
msgid ""
" -T <tags>        : Filter bugs by tags you want to see.\n"
msgstr ""
" -T <štítky>      : Zobrazí jen chyby s danými štítky.\n"

#: ../lib/aptlistbugs/logic.rb:57
msgid ""
" -S <states>      : Filter bugs by pending-state categories you want to see\n"
"                    [%s].\n"
msgstr ""
" -S <stavy>       : Zobrazí jen chyby daných stavů\n"
"                    [%s].\n"

#: ../lib/aptlistbugs/logic.rb:58
msgid ""
" -B <bug#>        : Filter bugs by number, showing only the specified bugs.\n"
msgstr ""
" -B <chyba#>      : Zobrazí pouze chyby daných čísel.\n"

#: ../lib/aptlistbugs/logic.rb:59
msgid ""
" -D               : Show downgraded packages, too.\n"
msgstr ""
" -D               : Vypíše také chyby v degradovaných balících.\n"

#: ../lib/aptlistbugs/logic.rb:60
msgid ""
" -H <hostname>    : Hostname of Debian Bug Tracking System [%s].\n"
msgstr ""
" -H <počítač>     : Jméno počítače na němž běží debianí BTS [%s].\n"

#: ../lib/aptlistbugs/logic.rb:61
msgid ""
" -p <port>        : Port number of the server [%s].\n"
msgstr ""
" -p <port>        : Číslo portu na serveru [%s].\n"

#: ../lib/aptlistbugs/logic.rb:62
msgid ""
" -P <priority>    : Pin-Priority value [%s].\n"
msgstr ""
" -P <priorita>    : Hodnota Pin-Priority [%s].\n"

#: ../lib/aptlistbugs/logic.rb:63
msgid ""
" -E <title>       : Title of RSS output.\n"
msgstr ""
" -E <titulek>     : Titulek RSS výstupu.\n"

#: ../lib/aptlistbugs/logic.rb:64
msgid ""
" -q               : Don't display progress bar.\n"
msgstr ""
" -q               : Nebude zobrazovat postup.\n"

#: ../lib/aptlistbugs/logic.rb:65
msgid ""
" -C <apt.conf>    : Additional apt.conf file to use.\n"
msgstr ""
" -C <apt.conf>    : Dodatečný soubor apt.conf, který se použije.\n"

#: ../lib/aptlistbugs/logic.rb:66
msgid ""
" -F               : Automatically pin all buggy packages.\n"
msgstr ""
" -F               : Automaticky vypíchne všechny chybové balíky.\n"

#: ../lib/aptlistbugs/logic.rb:67
msgid ""
" -N               : Never automatically pin packages.\n"
msgstr ""
" -N               : Nikdy automaticky nevypíchne balíky.\n"

#: ../lib/aptlistbugs/logic.rb:68
msgid ""
" -y               : Assume yes for all questions.\n"
msgstr ""
" -y               : Na všechny otázky použije odpověď ano.\n"

#: ../lib/aptlistbugs/logic.rb:69
msgid ""
" -n               : Assume no for all questions.\n"
msgstr ""
" -n               : Na všechny otázky použije odpověď ne.\n"

#: ../lib/aptlistbugs/logic.rb:70
msgid ""
" -a               : Assume the default reply for all questions.\n"
msgstr ""
" -y               : Na všechny otázky použije výchozí odpověď.\n"

#: ../lib/aptlistbugs/logic.rb:71
msgid ""
" -d               : Debug.\n"
msgstr ""
" -d               : Ladění.\n"

#: ../lib/aptlistbugs/logic.rb:72
msgid ""
" -h               : Display this help and exit.\n"
msgstr ""
" -h               : Zobrazí tuto nápovědu a skončí.\n"

#: ../lib/aptlistbugs/logic.rb:73
msgid ""
" -v               : Show version number and exit.\n"
msgstr ""
" -v               : Zobrazí číslo verze a skončí.\n"

#: ../lib/aptlistbugs/logic.rb:74
msgid ""
"Commands:\n"
msgstr ""
"Příkazy:\n"

#: ../lib/aptlistbugs/logic.rb:75
msgid ""
" apt              : Apt mode.\n"
msgstr ""
" apt              : Režim pro apt.\n"

#: ../lib/aptlistbugs/logic.rb:76
msgid ""
" list <pkg>..     : List bug reports of the specified packages.\n"
msgstr ""
" list <balík>..   : K zadaným balíkům vypíše seznam chyb.\n"

#: ../lib/aptlistbugs/logic.rb:77
msgid ""
" rss <pkg>..      : List bug reports of the specified packages in RSS.\n"
msgstr ""
" rss <balík>..    : K zadaným balíkům vypíše seznam chyb v RSS.\n"

#: ../lib/aptlistbugs/logic.rb:78
msgid ""
"See the manual page for the long options.\n"
msgstr ""
"Dlouhé varianty voleb naleznete v manuálové stránce.\n"

#. TRANSLATORS: the following six strings refer to a plural quantity of bugs
#. TRANSLATORS: please note that "Outstanding" means "unresolved", not "exceptional"
#: ../lib/aptlistbugs/logic.rb:87
msgid "Outstanding"
msgstr "Nevyřešeno"

#: ../lib/aptlistbugs/logic.rb:88
msgid "Forwarded"
msgstr "Přeposláno"

#: ../lib/aptlistbugs/logic.rb:89
msgid "Pending Upload"
msgstr "Stačí vydat"

#: ../lib/aptlistbugs/logic.rb:90
msgid "Fixed in NMU"
msgstr "Opraveno v NMU"

#: ../lib/aptlistbugs/logic.rb:91
msgid "From other Branch"
msgstr "Z jiné větve"

#: ../lib/aptlistbugs/logic.rb:92
msgid "Resolved in some Version"
msgstr "Opraveno v nějaké verzi"

#. TRANSLATORS: "W: " is a label for warnings; you may translate it with a suitable abbreviation of the word "warning"
#: ../lib/aptlistbugs/logic.rb:252 ../lib/aptlistbugs/logic.rb:259
#: ../lib/aptlistbugs/logic.rb:291 ../lib/aptlistbugs/logic.rb:404
#: ../lib/aptlistbugs/logic.rb:422 ../lib/aptlistbugs/logic.rb:524
#: ../lib/aptlistbugs/logic.rb:734 ../lib/aptlistbugs/logic.rb:936
msgid "W: "
msgstr "W: "

#: ../lib/aptlistbugs/logic.rb:252
msgid "Unrecognized severity '%s' will be ignored by the Debian BTS."
msgstr "Nerozpoznaná závažnost „%s“ bude ignorována."

#: ../lib/aptlistbugs/logic.rb:255
msgid "Bugs of severity %s"
msgstr "Chyby závažnosti %s"

#: ../lib/aptlistbugs/logic.rb:259
msgid ""
"sanity check failed: environment variable http_proxy is unset and HTTP_PROXY "
"is set."
msgstr ""
"kontrola selhala: proměnná prostředí http_proxy není nastavena, HTTP_PROXY je "
"nastavena."

#: ../lib/aptlistbugs/logic.rb:291
msgid "Cannot execute auto proxy detect command %s"
msgstr "Nelze spustit příkaz pro automatické rozpoznání proxy %s"

#: ../lib/aptlistbugs/logic.rb:310
msgid "You need to specify a command."
msgstr "Musíte zadat příkaz."

#: ../lib/aptlistbugs/logic.rb:320
msgid "Unknown command "
msgstr "Neznámý příkaz "

#: ../lib/aptlistbugs/logic.rb:404 ../libexec/aptcleanup:52
msgid "Cannot read from %s"
msgstr "Nelze číst z %s"

#: ../lib/aptlistbugs/logic.rb:422 ../lib/aptlistbugs/logic.rb:734
msgid "Cannot write to %s"
msgstr "Nelze zapisovat do %s"

#: ../lib/aptlistbugs/logic.rb:461
msgid "Are you sure you want to install/upgrade the above packages?"
msgstr "Opravdu chcete instalovat/aktualizovat výše zmíněné balíky?"

#: ../lib/aptlistbugs/logic.rb:513 ../lib/aptlistbugs/logic.rb:538
#: ../lib/aptlistbugs/logic.rb:604
msgid "%s is unknown"
msgstr "%s není známa"

#: ../lib/aptlistbugs/logic.rb:524
msgid "Failed to invoke querybts."
msgstr "Nepodařilo se spustit querybts."

#: ../lib/aptlistbugs/logic.rb:529
msgid "You must install the reportbug package to be able to do this"
msgstr "Abyste to mohli provést, musíte si nainstalovat balík reportbug"

#: ../lib/aptlistbugs/logic.rb:547
#, fuzzy
msgid "%s is unknown to the BTS"
msgstr "%s není známa"

#. TRANSLATORS: "ignored" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:556 ../lib/aptlistbugs/logic.rb:570
msgid "%s ignored"
msgstr "%s ignorována"

#. TRANSLATORS: "ignored" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:559
msgid "%s already ignored"
msgstr "%s již ignorována"

#. TRANSLATORS: %{plist} is a comma-separated list of %{npkgs} packages to be pinned.
#: ../lib/aptlistbugs/logic.rb:581 ../lib/aptlistbugs/logic.rb:660
msgid ""
"The following %{npkgs} package will be pinned:\n"
" %{plist}\n"
"Are you sure?"
msgid_plural ""
"The following %{npkgs} packages will be pinned:\n"
" %{plist}\n"
"Are you sure?"
msgstr[0] ""
"Následující balík bude vypíchnut:\n"
" %{plist}\n"
"Jste si jisti?"
msgstr[1] ""
"Následující %{npkgs} balíky budou vypíchnuty:\n"
" %{plist}\n"
"Jste si jisti?"
msgstr[2] ""
"Následujících %{npkgs} balíků bude vypíchnuto:\n"
" %{plist}\n"
"Jste si jisti?"

#. TRANSLATORS: %{blist} is a comma-separated list of %{nbugs} bugs to be dodged.
#: ../lib/aptlistbugs/logic.rb:619
msgid ""
"The following %{nbugs} bug will be dodged:\n"
" %{blist}\n"
"Are you sure?"
msgid_plural ""
"The following %{nbugs} bugs will be dodged:\n"
" %{blist}\n"
"Are you sure?"
msgstr[0] ""
"Vyhnete se následující chybě:\n"
" %{blist}\n"
"Jste si jisti?"
msgstr[1] ""
"Vyhnete se následujícím %{nbugs} chybám:\n"
" %{blist}\n"
"Jste si jisti?"
msgstr[2] ""
"Vyhnete se následujícím %{nbugs} chybám:\n"
" %{blist}\n"
"Jste si jisti?"

#: ../lib/aptlistbugs/logic.rb:638
msgid "You must install a web browser package to be able to do this"
msgstr "Abyste to mohli provést, musíte si nainstalovat webový prohlížeč"

#: ../lib/aptlistbugs/logic.rb:670
msgid "All selected packages are already pinned. Ignoring %s command."
msgstr "Všechny zvolené balíky jsou již vypíchnuty. Ignoruji příkaz %s."

#: ../lib/aptlistbugs/logic.rb:676
msgid "There are no dodge/pin/ignore operations to undo. Ignoring %s command."
msgstr ""
"Neexistuje žádné vyhnutí/vypíchnutí/ignorování, které by šlo vrátit. Ignoruji "
"příkaz %s."

#: ../lib/aptlistbugs/logic.rb:678
msgid ""
"All the dodge/pin/ignore operations will be undone.\n"
"Are you sure?"
msgstr ""
"Všechna vyhnutí/vypíchnutí/ignorování budou vrácena.\n"
"Jste si jistí?"

#. TRANSLATORS: the dashes (-) in the following strings are vertically aligned, please keep their alignment consistent
#: ../lib/aptlistbugs/logic.rb:691
msgid ""
"     y     - continue the APT installation.\n"
msgstr ""
"     y     - pokračuje v instalaci.\n"

#: ../lib/aptlistbugs/logic.rb:694
msgid ""
"     n     - stop the APT installation.\n"
msgstr ""
"     n     - zastaví instalaci.\n"

#. TRANSLATORS: %{prog} is the name of a program, %{user} is a user name.
#: ../lib/aptlistbugs/logic.rb:698
msgid ""
"   <num>   - query the specified bug number\n"
"             (uses %{prog} as user %{user}).\n"
msgstr ""
"   <čís>   - dotáže se na zadané číslo chyby\n"
"             (použije %{prog} pod uživatelem %{user}).\n"

#: ../lib/aptlistbugs/logic.rb:701
msgid ""
"  #<num>   - same as <num>.\n"
msgstr ""
"  #<čís>   - stejné jako <čís>.\n"

#: ../lib/aptlistbugs/logic.rb:702
msgid ""
"   b<id>   - same as <num>, but query the bug identified by <id>.\n"
msgstr ""
"   b<id>   - stejné jako <čís>, ale určí chybu pomocí identifikátoru <id>.\n"

#: ../lib/aptlistbugs/logic.rb:705
msgid ""
"     r     - redisplay bug lists.\n"
msgstr ""
"     r     - znovu vypíše seznamy chyb.\n"

#: ../lib/aptlistbugs/logic.rb:706
msgid ""
"     c     - compose bug lists in HTML.\n"
msgstr ""
"     c     - vytvoří seznamy chyb v HTML.\n"

#: ../lib/aptlistbugs/logic.rb:708
msgid ""
"     w     - display bug lists in HTML\n"
"             (uses %{prog} as user %{user}).\n"
msgstr ""
"     w     - zobrazí seznamy chyb v HTML\n"
"             (použije %{prog} pod uživatelem %{user}).\n"

#: ../lib/aptlistbugs/logic.rb:713
msgid ""
" d <num>.. - dodge bugs <num> by pinning affected packages\n"
"             (restart APT session to enable).\n"
msgstr ""
" d <čís>.. - vyhne se chybám <čís> vypíchnutím příslušných balíků\n"
"             (vyžaduje restart APT).\n"

#: ../lib/aptlistbugs/logic.rb:714
msgid ""
" d b<id>.. - dodge bugs identified by <id> by pinning affected packages\n"
"             (restart APT session to enable).\n"
msgstr ""
" d b<id>.. - vyhne se chybám <id> vypíchnutím příslušných balíků\n"
"             (vyžaduje restart APT).\n"

#: ../lib/aptlistbugs/logic.rb:715
msgid ""
" p <pkg>.. - pin packages <pkg>\n"
"             (restart APT session to enable).\n"
msgstr ""
" p <bal>.. - vypíchne balíky <bal>\n"
"             (vyžaduje restart APT).\n"

#: ../lib/aptlistbugs/logic.rb:716
msgid ""
" p         - pin all the above packages\n"
"             (restart APT session to enable).\n"
msgstr ""
" p         - vypíchne všechny výše uvedené balíky\n"
"             (vyžaduje restart APT).\n"

#: ../lib/aptlistbugs/logic.rb:717
msgid ""
" i <num>   - mark bug number <num> as ignored.\n"
msgstr ""
" i <čís>   - označí číslo chyby <čís> jako ignorované.\n"

#: ../lib/aptlistbugs/logic.rb:718
msgid ""
" i b<id>   - mark the bug identified by <id> as ignored.\n"
msgstr ""
" i b<id>   - označí chybu <id> jako ignorovanou.\n"

#: ../lib/aptlistbugs/logic.rb:719
msgid ""
" i         - mark all the above bugs as ignored.\n"
msgstr ""
" i         - označí všechny výše uvedené chyby jako ignorované.\n"

#: ../lib/aptlistbugs/logic.rb:720
msgid ""
"     u     - undo all the dodge/pin/ignore operations done so far.\n"
msgstr ""
"     u     - vrátí zpět všechny vyhnutí/vypíchnutí/ignorování.\n"

#: ../lib/aptlistbugs/logic.rb:721
msgid ""
"     ?     - print this help.\n"
msgstr ""
"     ?     - zobrazí tuto nápovědu.\n"

#: ../lib/aptlistbugs/logic.rb:764
msgid ""
"None of the above bugs is assigned to package %s\n"
"Are you sure you want to pin it?"
msgstr ""
"Žádné z uvedených chyb nejsou přiřazené balíku %s\n"
"Opravdu jej chcete vypíchnout?"

#. TRANSLATORS: %{packgl} is a list of packages.
#: ../lib/aptlistbugs/logic.rb:788
msgid "%{packgl} will be pinned. Restart APT session to enable"
msgstr "%{packgl} budou vypíchnuty. Vyžaduje restart APT"

#. TRANSLATORS: %{sevty} is the severity of some of the bugs found for package %{packg}.
#: ../lib/aptlistbugs/logic.rb:813
msgid "%{sevty} bugs of %{packg} ("
msgstr "%{sevty} chyby v %{packg} ("

#. TRANSLATORS: "Found" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:826
msgid " (Found: %s)"
msgstr " (Nalezeno: %s)"

#. TRANSLATORS: "Fixed" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:828
msgid " (Fixed: %s)"
msgstr " (Opraveno: %s)"

#. TRANSLATORS: "Merged" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:832 ../lib/aptlistbugs/logic.rb:998
msgid "   Merged with:"
msgstr "   Sloučeno s:"

#. TRANSLATORS: %{nbugs} is the number of bugs found for package %{packg}.
#: ../lib/aptlistbugs/logic.rb:847
msgid "%{packg}(%{nbugs} bug)"
msgid_plural "%{packg}(%{nbugs} bugs)"
msgstr[0] "%{packg}(%{nbugs} chyba)"
msgstr[1] "%{packg}(%{nbugs} chyby)"
msgstr[2] "%{packg}(%{nbugs} chyb)"

#: ../lib/aptlistbugs/logic.rb:854
msgid ""
"Summary:\n"
" "
msgstr ""
"Souhrn:\n"
" "

#. TRANSLATORS: this is a summary description of the structure of a table (for accessibility)
#: ../lib/aptlistbugs/logic.rb:867
msgid ""
"The top row describes the meaning of the columns; the other rows describe bug "
"reports, one per row"
msgstr ""
"Horní řádek popisuje význam sloupců, ostatní řádky popisují chyby, jedna "
"chyba na řádek"

#: ../lib/aptlistbugs/logic.rb:872 ../lib/aptlistbugs/logic.rb:992
msgid "package"
msgstr "balík"

#: ../lib/aptlistbugs/logic.rb:873
msgid "version change"
msgstr "změna verze"

#: ../lib/aptlistbugs/logic.rb:874 ../lib/aptlistbugs/logic.rb:993
msgid "severity"
msgstr "závažnost"

#: ../lib/aptlistbugs/logic.rb:875 ../lib/aptlistbugs/logic.rb:991
msgid "bug number"
msgstr "č. chyby"

#: ../lib/aptlistbugs/logic.rb:876
msgid "description"
msgstr "popis"

#: ../lib/aptlistbugs/logic.rb:894 ../lib/aptlistbugs/logic.rb:897
msgid "Relevant bugs for your upgrade"
msgstr "Relevantní chyby v této aktualizaci"

#: ../lib/aptlistbugs/logic.rb:898
msgid "by apt-listbugs"
msgstr "přináší apt-listbugs"

#: ../lib/aptlistbugs/logic.rb:928
msgid ""
"You can view the bug lists in HTML at the following URI:\n"
msgstr ""
"Seznamy chyb si můžete prohlédnout v HTML na následujícím URI:\n"

#: ../lib/aptlistbugs/logic.rb:936
msgid "Failed to invoke browser."
msgstr "Nepodařilo se spustit prohlížeč."

#: ../lib/aptlistbugs/logic.rb:994
msgid "category of bugs"
msgstr "kategorie chyb"

#: ../lib/aptlistbugs/logic.rb:995
msgid "tags"
msgstr "štítky"

#: ../lib/aptlistbugs/logic.rb:1027 ../lib/aptlistbugs/logic.rb:1115
#: ../lib/aptlistbugs/logic.rb:1324
msgid "Done"
msgstr "Hotovo"

#: ../lib/aptlistbugs/logic.rb:1039
msgid "Not Implemented"
msgstr "Neimplementováno"

#. TRANSLATORS: this sentence, followed by the translation of "Done" (see above) should fit in less than 79 columns to work well with default width terminals
#: ../lib/aptlistbugs/logic.rb:1061 ../lib/aptlistbugs/logic.rb:1067
#: ../lib/aptlistbugs/logic.rb:1112 ../lib/aptlistbugs/logic.rb:1115
msgid "Retrieving bug reports..."
msgstr "Stahuje se hlášení o chybách..."

#: ../lib/aptlistbugs/logic.rb:1070 ../lib/aptlistbugs/logic.rb:1082
#: ../lib/aptlistbugs/logic.rb:1094
msgid " Fail"
msgstr " Selhání"

#: ../lib/aptlistbugs/logic.rb:1072
msgid "HTTP GET failed"
msgstr "HTTP GET selhalo"

#: ../lib/aptlistbugs/logic.rb:1074 ../lib/aptlistbugs/logic.rb:1086
#: ../lib/aptlistbugs/logic.rb:1104
msgid "Retry downloading bug information?"
msgstr "Pokusit se o nové stažení informací o chybě?"

#: ../lib/aptlistbugs/logic.rb:1075 ../lib/aptlistbugs/logic.rb:1087
#: ../lib/aptlistbugs/logic.rb:1105
msgid "One package at a time?"
msgstr "Jeden balík po druhém?"

#: ../lib/aptlistbugs/logic.rb:1076 ../lib/aptlistbugs/logic.rb:1088
#: ../lib/aptlistbugs/logic.rb:1106
msgid "One bug report at a time?"
msgstr "Jednu chybu po druhé?"

#: ../lib/aptlistbugs/logic.rb:1079 ../lib/aptlistbugs/logic.rb:1091
#: ../lib/aptlistbugs/logic.rb:1109
msgid "Exiting with error"
msgstr "Končím s chybou"

#: ../lib/aptlistbugs/logic.rb:1079 ../lib/aptlistbugs/logic.rb:1091
#: ../lib/aptlistbugs/logic.rb:1109
msgid "Continue the installation anyway?"
msgstr "Přesto pokračovat v instalaci?"

#: ../lib/aptlistbugs/logic.rb:1084
msgid "Empty stream from SOAP"
msgstr "Prázdný proud ze SOAPu"

#: ../lib/aptlistbugs/logic.rb:1096
msgid "Error retrieving bug reports from the server with the following error message:"
msgstr "Chyba při stahování chybových hlášení ze serveru:"

#: ../lib/aptlistbugs/logic.rb:1099
msgid ""
"It appears that your network connection is down. Check network configuration "
"and try again"
msgstr ""
"Zdá se, že vaše síťové připojení nefunguje. Zkontrolujte nastavení sítě a "
"zkuste to znovu"

#: ../lib/aptlistbugs/logic.rb:1101
msgid ""
"It could be because your network is down, or because of broken proxy servers, "
"or the BTS server itself is down. Check network configuration and try again"
msgstr ""
"Může to být proto, že je vaše síťové připojení shozené, nebo jsou nefunkční "
"proxy servery, nebo neběží samotný BTS server. Zkontrolujte nastavení sítě a "
"zkuste to znovu"

#. TRANSLATORS: this sentence, followed by the translation of "Done" (see above) should fit in less than 79 columns to work well with default width terminals
#: ../lib/aptlistbugs/logic.rb:1290 ../lib/aptlistbugs/logic.rb:1310
#: ../lib/aptlistbugs/logic.rb:1323 ../lib/aptlistbugs/logic.rb:1324
msgid "Parsing Found/Fixed information..."
msgstr "Zpracovávají se nalezené informace..."

#: ../libexec/aptcleanup:123
msgid "Fixed packages : "
msgstr "Opravené balíky: "

#~ msgid ""
#~ " list <pkg...>    : List bug reports of the specified packages.\n"
#~ msgstr ""
#~ " list <balík...>  : K zadaným balíkům vypíše seznam chyb.\n"
#~ msgid ""
#~ " rss <pkg...>     : List bug reports of the specified packages in RSS.\n"
#~ msgstr ""
#~ " rss <balík...>   : K zadaným balíkům vypíše seznam chyb v RSS.\n"
#~ msgid ""
#~ " p <pkg>.. - pin pkgs\n"
#~ "             (restart APT session to enable).\n"
#~ msgstr ""
#~ " p <bal..> - vypíchne balíky (vyžaduje restart APT).\n"
#~ msgid ""
#~ " p         - pin all the above pkgs\n"
#~ "             (restart APT session to enable).\n"
#~ msgstr ""
#~ " p         - vypíchne všechny výše uvedené balíky (vyžaduje restart APT).\n"
#~ msgid ""
#~ "APT Pre-Install-Pkgs is not giving me expected 'VERSION 3' string.\n"
#~ msgstr ""
#~ "APT Pre-Install-Pkgs nevrací očekávaný řetězec „VERSION 3“.\n"
#~ msgid ""
#~ "APT Pre-Install-Pkgs is giving me fewer fields than expected.\n"
#~ msgstr ""
#~ "APT Pre-Install-Pkgs vrací méně polí, než je očekáváno.\n"
#~ msgid ""
#~ "APT Pre-Install-Pkgs is giving me an invalid direction of version change.\n"
#~ msgstr ""
#~ "APT Pre-Install-Pkgs vrací neplatný směr změny verze.\n"
#~ msgid "********** on_hold IS DEPRECATED. USE p INSTEAD to use pin **********"
#~ msgstr "******* on_hold JE ZASTARALÉ. Použijte vypichování pomocí p. ******"
#~ msgid ""
#~ "The following %{npkgs} package will be pinned or on hold:\n"
#~ " %{plist}\n"
#~ "Are you sure?"
#~ msgid_plural ""
#~ "The following %{npkgs} packages will be pinned or on hold:\n"
#~ " %{plist}\n"
#~ "Are you sure?"
#~ msgstr[0] ""
#~ "Následující balík bude vypíchnut nebo podržen:\n"
#~ " %{plist}\n"
#~ "Jste si jisti?"
#~ msgstr[1] ""
#~ "Následující %{npkgs} balíky budou vypíchnuty nebo podrženy:\n"
#~ " %{plist}\n"
#~ "Jste si jisti?"
#~ msgstr[2] ""
#~ "Následujících %{npkgs} balíků bude vypíchnuto nebo podrženo:\n"
#~ " %{plist}\n"
#~ "Jste si jisti?"
#~ msgid "All selected packages are already pinned or on hold. Ignoring %s command."
#~ msgstr "Všechny zvolené balíky jsou již vypíchnuty nebo podrženy. Ignoruji příkaz %s."
#~ msgid ""
#~ "     y     - continue the APT installation, but do not mark the bugs\n"
#~ "             as ignored.\n"
#~ msgstr ""
#~ "     y     - pokračuje v instalaci, ale neoznačí uvedené chyby jako "
#~ "ignorované.\n"
#~ msgid ""
#~ "     a     - continue the APT installation and mark all the above bugs\n"
#~ "             as ignored.\n"
#~ msgstr ""
#~ "     a     - pokračuje v instalaci a označí uvedené chyby jako ignorované.\n"
#~ msgid ""
#~ " p <pkg..> - pin pkgs (restart APT session to enable).\n"
#~ msgstr ""
#~ " p <bal..> - vypíchne balíky (vyžaduje restart APT).\n"
#~ msgid ""
#~ " p         - pin all the above pkgs (restart APT session to enable).\n"
#~ msgstr ""
#~ " p         - vypíchne všechny výše uvedené balíky (vyžaduje restart APT).\n"
#~ msgid ""
#~ "%{packgl} pinned by adding Pin preferences in %{filenm}. Restart APT session "
#~ "to enable"
#~ msgstr ""
#~ "Balíky %{packgl} byly vypíchnuty nastavením v %{filenm}. Aby se změny "
#~ "projevily, musíte restartovat APT"
#~ msgid "%s held. Restart APT session to enable"
#~ msgstr "Balíky %s byly podrženy. Aby se změny projevily, musíte restartovat APT"
#~ msgid ""
#~ " -y               : Assume that you select yes for all questions.\n"
#~ msgstr ""
#~ " -y               : Bude předpokládat, že na všechny otázky odpovíte ano.\n"
#~ msgid ""
#~ " -n               : Assume that you select no for all questions.\n"
#~ msgstr ""
#~ " -n               : Bude předpokládat, že na všechny otázky odpovíte ne.\n"
#~ msgid ""
#~ "   <num>   - query the specified bug number (requires reportbug).\n"
#~ msgstr ""
#~ "   <čís>   - dotáže se na zadané číslo chyby (vyžaduje reportbug).\n"
#~ msgid ""
#~ "     w     - display bug lists in HTML (uses %s).\n"
#~ msgstr ""
#~ "     w     - zobrazí seznam chyb v HTML (pomocí %s).\n"
#~ msgid ""
#~ " -C <apt.conf>    : apt.conf file to use.\n"
#~ msgstr ""
#~ " -C <apt.conf>    : Soubor apt.conf, který se má použít.\n"
#~ msgid "W: cannot open /dev/tty - running inside su -c \"command\"? Switching to non-interactive failure mode (see /usr/share/doc/apt-listbugs/README.Debian.gz)"
#~ msgstr "W: nelze otevřít /dev/tty - spuštěno uvnitř su -c \"příkaz\"? Přepínám se do neinteraktivního režimu (viz /usr/share/doc/apt-listbugs/README.Debian.gz)"
#~ msgid "Bug reports which are marked as %s in the bug tracking system"
#~ msgstr "Hlášení o chybách označená v systému sledování chyb jako %s"
#~ msgid "Bug reports"
#~ msgstr "Hlášení o chybách"
#~ msgid "Package upgrade information in question"
#~ msgstr "Informace o aktualizaci zvolených balíků"
#~ msgid "unfixed"
#~ msgstr "neopravena"
#~ msgid "tagged as pending a fix"
#~ msgstr "označena jako čekající oprava"
#~ msgid "W: sanity check failed: environment variable http_proxy is set and soap_use_proxy is not 'on'."
#~ msgstr "W: kontrola selhala: proměnná prostředí http_proxy je nastavena, soap_use_proxy nemá hodnotu 'on'."
#~ msgid "%s(%d bug)"
#~ msgid_plural "%s(%d bugs)"
#~ msgstr[0] "%s (chyb: %d)"
#~ msgstr[1] "%s (chyb: %d)"
#~ msgid "Reading package fields..."
#~ msgstr "Čte se pole v balíku..."
#~ msgid "Reading package status..."
#~ msgstr "Čte se stav balíku..."
#~ msgid "in the bug tracking system</caption>"
#~ msgstr "v systému sledování chyb</caption>"
#~ msgid "%s(%s bugs)"
#~ msgstr "%s (chyb: %s)"
#~ msgid " -f               : Retrieve bug reports from BTS forcibly.\n"
#~ msgstr " -f               : Vynutí stažení chybových hlášení přímo z BTS.\n"
#~ msgid "<html><head><title>critical bugs for your upgrade</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=#{Locale::LangInfo.langinfo(Locale::LangInfo::CODESET)}\"></head><body>"
#~ msgstr "<html><head><title>kritické chyby v této aktualiaci</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=#{Locale::LangInfo.langinfo(Locale::LangInfo::CODESET)}\"></head><body>"
#~ msgid " --indexdir       : Directory where index.db located [%s]\n"
#~ msgstr " --indexdir       : Adresář, ve kterém leží index.db [%s]\n"
#~ msgid " -c <dir>         : Specify cache_dir [%s].\n"
#~ msgstr " -c <dir>         : Určí adresář s lokální cache [%s].\n"
#~ msgid " -t <minutes>     : Specify cache expire timer in minutes"
#~ msgstr " -t <minuty>      : Určí čas vypršení platnosti cache (v minutách)"
#~ msgid "Error retrieving bug reports"
#~ msgstr "Chyba při stahování hlášení o chybách"
#~ msgid " -l               : Show bugs already existed in your system.\n"
#~ msgstr " -l               : Vypíše také chyby, které již máte v systému.\n"
#~ msgid "Retrieving release-critical bug reports..."
#~ msgstr "Stahuji hlášení o kritických chybách"
#~ msgid "Retrieving extra bug reports..."
#~ msgstr "Stahuji další hlášení o chybách..."
#~ msgid "Newly installation package '%s' ignored"
#~ msgstr "Ignoruji nově instalovaný balík '%s'"
#~ msgid " -R               : Use Release-Critical bug reports\n"
#~ msgstr " -R               : Použije seznam kritických chyb\n"
#~ msgid "can't be used -s and -R together"
#~ msgstr "-s a -R nelze použít současně"
#~ msgid "Too many errors while retrieving bug reports"
#~ msgstr "Příliš mnoho chyb při stahování hlášení o chybách"
#~ msgid "Bugs of the following packages couldn't be fetched:"
#~ msgstr "Nepodařilo se stáhnout chyby v následujících balících:"
#~ msgid "Assuming that there are no grave bugs in these packages."
#~ msgstr "Předpokládám, že v těchto balících nejsou žádné vážné chyby."
#~ msgid "Are you sure"
#~ msgstr "Jste si jisti"
