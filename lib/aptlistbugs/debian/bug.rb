#
# bug.rb - ruby interface for debian bug
# Copyright (C) 2002       Masato Taruishi <taru@debian.org>
# Copyright (C) 2006-2007  Junichi Uekawa <dancer@debian.org>
# Copyright (C) 2013-2020  Francesco Poli <invernomuto@paranoici.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License with
# the Debian GNU/Linux distribution in file /usr/share/common-licenses/GPL-2;
# if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#

module Debian
  class Bug

    def initialize (pkg_key, bug_number, severity,
                    stat, desc, tags = [], mergeids = [], time = Time.now )
      @pkg_key = pkg_key
      @bug_number = bug_number
      @severity = severity
      @stat = stat
      @desc = desc
      @tags = tags
      @mergeids = mergeids
      @log = nil
      @time = time
      @found = nil
      @fixed = nil
      @id = nil
    end

    def to_s
      "#" + @bug_number + ":" + @pkg_key + ":" + @desc
    end

    def inspect
      @bug_number + " - " +
        @pkg_key + " - " +
        @severity + " - " +
        @stat + " - " +
        @tags.to_s + " - " +
        @desc + " - merged with: " + @mergeids.join(', ') + " - " +
        @found.to_s + " - " +
        @fixed.to_s + " - " +
        @time.to_s + " - b" +
        @id.to_s
    end

    attr_accessor :pkg_key, :bug_number, :severity,
    :stat, :desc, :tags, :mergeids, :log, :time, :found, :fixed, :id

  end

  class Bugs < Array

    def each_by_category (pkg_key, sev, stat)
      each { |bug|
        yield bug if bug.pkg_key == pkg_key &&
          bug.severity == sev &&
          bug.stat == stat
      }
    end

    def sub(type, val)
      sub = Bugs.new
      each { |bug|
        sub << bug if bug.send(type) == val
      }
      sub
    end

    def filter_out(bugnums)
      fout = Bugs.new
      each { |bug|
        fout << bug unless bugnums.include?(bug.bug_number)
      }
      fout
    end

    def filter_in(bugnums)
      fin = Bugs.new
      each { |bug|
        fin << bug if bugnums.include?(bug.bug_number)
      }
      fin
    end

    def extract(bugnum)
      i = index { |b| b.bug_number == bugnum }
      if i.nil?
        nil
      else
        at(i)
      end
    end
  end
end
