LINGUAS := $(shell cd po; ls *.po| cut -d'.' -f1)
MOFILES := $(patsubst %.po,%.mo,$(wildcard po/*.po))
VERSION := $(shell dpkg-parsechangelog -SVersion)
YEAR := $(shell dpkg-parsechangelog -SDate | cut -d ' ' -f 4)

CEX := po/.*\.mo
CEX := $(CEX)|apt-listbugs\.1
CEX := $(CEX)|debian/\.debhelper/
CEX := $(CEX)|debian/debhelper-build-stamp
CEX := $(CEX)|debian/apt-listbugs\.postrm\.debhelper
CEX := $(CEX)|debian/apt-listbugs\.debhelper\.log
CEX := $(CEX)|debian/apt-listbugs\.substvars
CEX := $(CEX)|debian/apt-listbugs/
CEX := $(CEX)|debian/files

.PHONY: all
all: apt-listbugs.1 $(MOFILES)

.PHONY: clean
clean:
	find -name '*~' -delete
	rm -f apt-listbugs.1
	rm -f $(MOFILES)

.PHONY: check
check:
	./test/test_logic.rb

apt-listbugs.1: bin/apt-listbugs
	cd bin/ && rd2 -r rd/rd2man-lib.rb -o ../apt-listbugs apt-listbugs

.PHONY: update-po
update-po: po/apt-listbugs.pot
	for po in $(LINGUAS); do \
	  rmsgmerge --wrap -U po/$$po.po po/apt-listbugs.pot; \
	done
	for po in $(LINGUAS); do \
	  msgfmt --statistics -v -c po/$$po.po -o /dev/null; \
	done

po/apt-listbugs.pot: bin/apt-listbugs lib/aptlistbugs/logic.rb \
                     libexec/aptcleanup
	rxgettext --add-comments=TRANSLATORS --wrap \
	          --copyright-holder="apt-listbugs authors" \
	          --copyright-year=2002-$(YEAR) \
	          --package-name=apt-listbugs --package-version=$(VERSION) \
	          --msgid-bugs-address=$$DEBEMAIL \
	          bin/apt-listbugs lib/aptlistbugs/logic.rb \
	          libexec/aptcleanup \
	          -o po/apt-listbugs.pot

.PHONY: call-for-translation
call-for-translation:
	podebconf-report-po --call --withtranslators \
	                    --conf po/.podebconf-report-po.conf

.PHONY: install
install:
	# Add here commands to install the package into debian/apt-listbugs.
	for mo in $(MOFILES); do \
	  install -d $(DESTDIR)/usr/share/locale/`basename $$mo .mo`/LC_MESSAGES; \
	  install -m644 $$mo $(DESTDIR)/usr/share/locale/`basename $$mo .mo`/LC_MESSAGES/apt-listbugs.mo; \
	done

%.mo: %.po
	msgfmt -o $@ $<

.PHONY: debian-copyright
debian-copyright:
	debclean && decopy --exclude '$(CEX)' --output debian/copyright
